# Use the official image as a parent image.
FROM rosenhoff/streamlit-demo-self-driving

# Set the working directory.
#WORKDIR /usr/src/app

# Copy the file from your host to your current location.
#COPY package.json .

# Run the command inside your image filesystem.
#RUN npm install

#RUN -d -p 8501:8501 rosenhoff/streamlit-demo-self-driving
# Add metadata to the image to describe which port the container is listening on at runtime.
EXPOSE 8501

# Run the specified command within the container.
#CMD [ "npm", "start" ]
