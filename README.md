# Self driving car recognition system
### Features

- Extremely easy deployment
- [web-app](https://github.com/Redocly/redoc/blob/master/cli/README.md) with ability to add your own training weights  into an LFS system in Gitlab
- Server Side Rendering ready
- The widest OpenAPI v2.0 features support (yes, it supports even `discriminator`) <br>
![](docs/images/discriminator-demo.gif)
- OpenAPI 3.0 support
- Neat **interactive** documentation for nested objects <br>


## Version Guidance
| Detection System Release | OpenAPI Specification |
|:--------------|:----------------------|
| 2.0.0.        | 3.0, 2.0              |
| 1.19.x        | 2.0                   |
| 1.18.x        | 2.0                   |
